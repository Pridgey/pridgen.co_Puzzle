﻿using Pridgenco.Content.puzzle;
using Pridgenco.Models.Puzzle;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Pridgenco.Controllers
{
	public class PuzzleController : Controller
	{
		// GET: Puzzle
		public ActionResult Index(string answer)
		{
			string puzzleConnectionString = WebConfigurationManager.ConnectionStrings["puzzledb"].ConnectionString;

			HomeModel home = new HomeModel();

			home.weekOf = (string)PuzzleHelper.BuildDateIdentifier(false);

			home.difficulty = PuzzleHelper.CalculateDifficulty(PuzzleHelper.GetCurrentPuzzleID());

			PuzzleModel puzzleModel = new PuzzleModel();

			puzzleModel.Puzzle = new PuzzleObject(PuzzleHelper.GetCurrentPuzzleID());

			FormModel formModel = new FormModel();
			formModel.hasAnswer = !string.IsNullOrEmpty(answer);
			if (formModel.hasAnswer)
			{
				formModel.CheckAnswer(answer);

				string identifier = GetIdentifyingCookie();

				if (string.IsNullOrEmpty(identifier))
				{
					identifier = SetIdentifyingCookie();
				}

				PuzzleHelper.InsertAnswerStat(answer, formModel.answerCorrect, identifier);
			}

			home.puzzleModel = puzzleModel;
			home.formModel = formModel;

			return View(home);
		}

		public ActionResult Stats()
		{
			StatsModel sm = new StatsModel();

			return View(sm);
		}

		public ActionResult Previous()
		{
			PreviousModel pm = new PreviousModel();

			return View(pm);
		}

		#region CookieManagement

		public string GetIdentifyingCookie()
		{
			string result = "";

			HttpCookie cookie = Request.Cookies["Ident"];
			if (cookie != null)
			{
				if (!string.IsNullOrEmpty(cookie.Value))
				{
					result = cookie.Value.ToString();
				}
			}

			return result;
		}

		public string SetIdentifyingCookie()
		{
			HttpCookie cookie = new HttpCookie("Ident");
			string result = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Request.UserHostAddress.ToString()));
			cookie.Value = result.Substring(0, result.Length > 499 ? 499 : result.Length);
			cookie.Expires = DateTime.Now.AddYears(2);
			Response.Cookies.Add(cookie);

			return cookie.Value;
		}

		#endregion CookieManagement
	}
}