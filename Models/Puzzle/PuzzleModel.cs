﻿using Pridgenco.Content.puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pridgenco.Models.Puzzle
{
	public class PuzzleModel
	{
		public PuzzleObject Puzzle { get; set; }
		public bool ShowHint = (DateTime.Today.DayOfWeek == DayOfWeek.Friday || DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday);
	}
}