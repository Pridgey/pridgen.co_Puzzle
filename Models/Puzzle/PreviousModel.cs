﻿using Pridgenco.Content.puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pridgenco.Models.Puzzle
{
	public class PreviousModel
	{
		public PuzzleModel previousPuzzle { get; set; }
		public PuzzleSolution previousSolutions { get; set; }
		public string weekOf { get; set; }
		public int numberOfGuesses { get; set; }
		public int numberOfCorrectGuesses { get; set; }
		public string finalDifficulty { get; set; }
		public PercentageCircleModel percentCircle { get; set; }

		public PreviousModel()
		{
			previousPuzzle = new PuzzleModel();
			previousPuzzle.Puzzle = new PuzzleObject(PuzzleHelper.GetPreviousPuzzleID());
			previousSolutions = new PuzzleSolution(PuzzleHelper.GetPreviousPuzzleID());
			weekOf = ((DateTime)PuzzleHelper.BuildDateIdentifier(true)).AddDays(-7).ToString("MMM dd");
			numberOfGuesses = PuzzleHelper.CalculatePuzzleAnswers(PuzzleHelper.GetPreviousPuzzleID());
			numberOfCorrectGuesses = PuzzleHelper.CalculatePuzzleCorrectAnswers(PuzzleHelper.GetPreviousPuzzleID());
			finalDifficulty = PuzzleHelper.CalculateDifficulty(PuzzleHelper.GetPreviousPuzzleID());
			percentCircle = new PercentageCircleModel();
			percentCircle.Percentage = numberOfGuesses != 0 ? (int)((double)numberOfCorrectGuesses / numberOfGuesses * 100) : 0;
		}
	}
}