﻿using Pridgenco.Content.puzzle;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Pridgenco.Models.Puzzle
{
	public class FormModel
	{
		public bool hasAnswer { get; set; }
		public bool answerCorrect { get; set; }

		public void CheckAnswer(string answer)
		{
			answerCorrect = false;
			answer = answer.ToLower();

			int puzzleID = PuzzleHelper.GetCurrentPuzzleID();

			PuzzleSolution solutions = new PuzzleSolution(puzzleID);

			foreach (string s in solutions.Solutions)
			{
				string singleAnswer = s.ToLower();

				if (answer.Contains(singleAnswer))
				{
					answerCorrect = true;
				}
			}
		}
	}
}