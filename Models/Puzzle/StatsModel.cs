﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pridgenco.Content.puzzle;

namespace Pridgenco.Models.Puzzle
{
	public class StatsModel
	{
		public int CurrentPuzzleAnswers { get; set; }
		public int CurrentPuzzleCorrectAnswers { get; set; }
		public int TotalAnswers { get; set; }
		public int TotalCorrectAnswers { get; set; }
		public string CurrentPuzzleDifficulty { get; set; }
		public PuzzleModel EasiestPuzzle { get; set; }
		public PuzzleModel HardestPuzzle { get; set; }
		public PercentageCircleModel CurrentPuzzlePercentageCircle { get; set; }
		public PercentageCircleModel AllPuzzlesPercentageCircle { get; set; }

		public StatsModel()
		{
			CurrentPuzzleAnswers = PuzzleHelper.CalculatePuzzleAnswers(PuzzleHelper.GetCurrentPuzzleID());
			CurrentPuzzleCorrectAnswers = PuzzleHelper.CalculatePuzzleCorrectAnswers(PuzzleHelper.GetCurrentPuzzleID());
			TotalAnswers = PuzzleHelper.CalculateAllAnswers();
			TotalCorrectAnswers = PuzzleHelper.CalculateAllCorrectAnswers();
			EasiestPuzzle = new PuzzleModel();
			HardestPuzzle = new PuzzleModel();
			EasiestPuzzle.Puzzle = new PuzzleObject(PuzzleHelper.CalculateEasiestPuzzle());
			HardestPuzzle.Puzzle = new PuzzleObject(PuzzleHelper.CalculateHardestPuzzle());

			CurrentPuzzleDifficulty = PuzzleHelper.CalculateDifficulty(PuzzleHelper.GetCurrentPuzzleID());

			CurrentPuzzlePercentageCircle = new PercentageCircleModel();
			CurrentPuzzlePercentageCircle.Percentage = CurrentPuzzleAnswers != 0 ? (int)((double)CurrentPuzzleCorrectAnswers / CurrentPuzzleAnswers * 100) : 0;

			AllPuzzlesPercentageCircle = new PercentageCircleModel();
			AllPuzzlesPercentageCircle.Percentage = TotalAnswers != 0 ? (int)((double)TotalCorrectAnswers / TotalAnswers * 100) : 0;
		}
	}
}