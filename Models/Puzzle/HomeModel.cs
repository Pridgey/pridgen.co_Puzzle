﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pridgenco.Models.Puzzle
{
	public class HomeModel
	{
		public string weekOf { get; set; }

		public string difficulty { get; set; }
		public PuzzleModel puzzleModel { get; set; }
		public FormModel formModel { get; set; }
	}
}