﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Pridgenco.Content.puzzle
{
	public class PuzzleObject
	{
		private static readonly string puzzleConnectionString = WebConfigurationManager.ConnectionStrings["puzzledb"].ConnectionString;

		public int PuzzleID;
		public string PuzzleTitle;
		public bool IsImagePuzzle;
		public string PuzzleWeek;
		public List<PuzzleItem> PuzzleItems;

		public PuzzleObject(int puzzleID)
		{
			PuzzleItems = new List<PuzzleItem>();

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT TOP 1 * FROM Puzzle WHERE PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							this.PuzzleID = puzzleID;
							this.PuzzleTitle = sqlReader.GetString(1);
							this.IsImagePuzzle = sqlReader.GetBoolean(2);
						}
					}
				}

				using (SqlCommand sqlCommand = new SqlCommand("SELECT * FROM PuzzleItem WHERE PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							PuzzleItem item = new PuzzleItem() { Item = sqlReader.GetString(1), Hint = sqlReader.GetString(2) };
							this.PuzzleItems.Add(item);
						}
					}
				}

				sqlConnection.Close();
			}
		}
	}
}