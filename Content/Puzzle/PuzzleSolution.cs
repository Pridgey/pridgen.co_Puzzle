﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Pridgenco.Content.puzzle
{
	public class PuzzleSolution
	{
		private static readonly string puzzleConnectionString = WebConfigurationManager.ConnectionStrings["puzzledb"].ConnectionString;
		public List<string> Solutions { get; set; }

		public PuzzleSolution(int puzzleID)
		{
			Solutions = new List<string>();

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT Solution FROM PuzzleSolution WHERE PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							Solutions.Add(sqlReader.GetString(0));
						}
					}
				}

				sqlConnection.Close();
			}
		}
	}
}