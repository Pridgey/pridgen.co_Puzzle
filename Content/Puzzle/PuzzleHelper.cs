﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Pridgenco.Content.puzzle
{
	public static class PuzzleHelper
	{
		private static readonly string puzzleConnectionString = WebConfigurationManager.ConnectionStrings["puzzledb"].ConnectionString;

		public static object BuildDateIdentifier(bool asDate)
		{
			DateTime dt = DateTime.Today;
			int diff = (7 + (dt.DayOfWeek - DayOfWeek.Monday)) % 7;
			DateTime day = dt.AddDays(-1 * diff);

			if (asDate)
			{
				return day;
			}
			else
			{
				string result = DateTime.Today.ToString("MMM") + " " + day.ToString("dd");

				return result;
			}
		}

		public static int GetCurrentPuzzleID()
		{
			DateTime dateStart = (DateTime)BuildDateIdentifier(true);
			DateTime dateEnd = dateStart.AddDays(7);
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT TOP 1 PuzzleID FROM Puzzle WHERE DateStart >= @dateStart AND DateEnd < @dateEnd;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("dateStart", dateStart));
					sqlCommand.Parameters.Add(new SqlParameter("dateEnd", dateEnd));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int GetPreviousPuzzleID()
		{
			DateTime dateEnd = (DateTime)BuildDateIdentifier(true);
			DateTime dateStart = dateEnd.AddDays(-7);
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT TOP 1 PuzzleID FROM Puzzle WHERE DateStart >= @dateStart AND DateEnd < @dateEnd;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("dateStart", dateStart));
					sqlCommand.Parameters.Add(new SqlParameter("dateEnd", dateEnd));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static void LogAnswer(string logText)
		{
			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO PuzzleLog (LogDate, LogMessage) VALUES (@date, @message);", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("date", DateTime.Now));
					sqlCommand.Parameters.Add(new SqlParameter("message", logText));

					sqlCommand.ExecuteNonQuery();
				}

				sqlConnection.Close();
			}
		}

		public static void InsertAnswerStat(string answer, bool answerCorrect, string identifier)
		{
			int puzzleID = GetCurrentPuzzleID();

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO Answer (PuzzleID, Correct, Answer, Identifier) VALUES (@puzzleid, @correct, @answer, @ident);", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));
					sqlCommand.Parameters.Add(new SqlParameter("correct", answerCorrect));
					sqlCommand.Parameters.Add(new SqlParameter("answer", answer));
					sqlCommand.Parameters.Add(new SqlParameter("ident", identifier));

					sqlCommand.ExecuteNonQuery();
				}

				sqlConnection.Close();
			}
		}

		public static string CalculateDifficulty(int puzzleID)
		{
			int countCorrectUnique = 0;
			int countTotalUnique = 1;
			int dayOfWeek;
			int scalability = 5;
			string difficulty = "Difficulty Unknown";

			dayOfWeek = (int)DateTime.Now.DayOfWeek;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM (SELECT DISTINCT Identifier FROM Answer WHERE Correct = 1 AND PuzzleID = @puzzleid) as SubTable;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							countCorrectUnique = sqlReader.GetInt32(0);
						}
					}
				}

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) FROM (SELECT DISTINCT Identifier FROM Answer WHERE PuzzleID = @puzzleid) as SubTable;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							countTotalUnique = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			double correct = Convert.ToDouble(countCorrectUnique);
			double total = Convert.ToDouble(countTotalUnique > 0 ? countTotalUnique : 1);
			double day = Convert.ToDouble(dayOfWeek > 0 ? dayOfWeek : 1);
			if (day > 5) day = 5;
			double constant = Convert.ToDouble(scalability);

			double correctPercentage = correct / total;
			double dayModifier = constant / day;
			double percentage = correctPercentage * dayModifier;

			if (percentage < 0.3f)
			{
				difficulty = "Hard";
			}
			if (percentage >= 0.3f && percentage <= 0.7f)
			{
				difficulty = "Medium";
			}
			if (percentage > 0.7f)
			{
				difficulty = "Easy";
			}

			return difficulty;
		}

		public static string CalculateDifficultyOld(int puzzleID)
		{
			int numberOfAnswers = 0;
			int correctAnswers = 0;
			string difficulty = "Difficulty Unknown";

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(AnswerID) FROM Answer WHERE PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							numberOfAnswers = sqlReader.GetInt32(0);
						}
					}
				}

				if (numberOfAnswers < 10)
				{
					difficulty = "Difficulty Unknown";
				}
				else
				{
					using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(AnswerID) FROM Answer WHERE PuzzleID = @puzzleid AND Correct = 1;", sqlConnection))
					{
						sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

						using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
						{
							while (sqlReader.Read())
							{
								correctAnswers = sqlReader.GetInt32(0);
							}
						}
					}

					double correctAnswersD = Convert.ToDouble(correctAnswers);
					double numberOfAnswersD = Convert.ToDouble(numberOfAnswers);
					double percentageCorrect = correctAnswersD / numberOfAnswersD;

					if (percentageCorrect > 0.7f)
					{
						difficulty = "Easy";
					}
					else if (percentageCorrect < 0.7f && percentageCorrect > 0.4f)
					{
						difficulty = "Medium";
					}
					else
					{
						difficulty = "Hard";
					}
				}

				sqlConnection.Close();
			}

			return difficulty;
		}

		public static int CalculatePuzzleAnswers(int puzzleID)
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(Answer.AnswerID) FROM Answer WHERE Answer.PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int CalculatePuzzleCorrectAnswers(int puzzleID)
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(Answer.AnswerID) FROM Answer WHERE Answer.Correct = 1 AND Answer.PuzzleID = @puzzleid;", sqlConnection))
				{
					sqlCommand.Parameters.Add(new SqlParameter("puzzleid", puzzleID));

					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int CalculateAllAnswers()
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(Answer.AnswerID) FROM Answer;", sqlConnection))
				{
					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int CalculateAllCorrectAnswers()
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(Answer.AnswerID) FROM Answer WHERE Answer.Correct = 1;", sqlConnection))
				{
					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int CalculateEasiestPuzzle()
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT TOP 1 PuzzleID, (((SELECT COUNT(AnswerID) FROM Answer WHERE Answer.PuzzleID = PA.PuzzleID AND Correct = 1) * 100)/(SELECT COUNT(AnswerID) FROM Answer WHERE Answer.PuzzleID = PA.PuzzleID)) AS 'Percent' FROM Answer PA GROUP BY PA.PuzzleID ORDER BY 'Percent' DESC;", sqlConnection))
				{
					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}

		public static int CalculateHardestPuzzle()
		{
			int result = 0;

			using (SqlConnection sqlConnection = new SqlConnection(puzzleConnectionString))
			{
				sqlConnection.Open();

				using (SqlCommand sqlCommand = new SqlCommand("SELECT TOP 1 PuzzleID, (((SELECT COUNT(AnswerID) FROM Answer WHERE Answer.PuzzleID = PA.PuzzleID AND Correct = 1) * 100)/(SELECT COUNT(AnswerID) FROM Answer WHERE Answer.PuzzleID = PA.PuzzleID)) AS 'Percent' FROM Answer PA GROUP BY PA.PuzzleID ORDER BY 'Percent' ASC;", sqlConnection))
				{
					using (SqlDataReader sqlReader = sqlCommand.ExecuteReader())
					{
						while (sqlReader.Read())
						{
							result = sqlReader.GetInt32(0);
						}
					}
				}

				sqlConnection.Close();
			}

			return result;
		}
	}
}